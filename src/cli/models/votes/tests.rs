use crate::cli::models::votes::*;

#[test]
fn can_record_yes_vote() {
    // Given a fresh pile for votes
    let mut votes = VotesData::new();
    assert_eq!(votes.yea_voters.len(), 0);

    // When we get a yes vote
    votes.record_vote("Alexander Hamilton".to_string(), true);

    // Then it goes in the yes pile
    assert_eq!(votes.yea_voters.len(), 1);
}

#[test]
fn can_record_no_vote() {
    // Given a fresh pile for votes
    let mut votes = VotesData::new();
    assert_eq!(votes.nay_voters.len(), 0);

    // When we get a no vote
    votes.record_vote("Aaron Burr, Sir".to_string(), false);

    // Then it goes in the no pile
    assert_eq!(votes.nay_voters.len(), 1);
}
