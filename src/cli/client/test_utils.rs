use lazy_static::lazy_static;
use xand_address::Address;
use xand_api_client::{
    AdministrativeTransaction, Proposal, ProposalStage, RegisterAccountAsMember,
};

lazy_static! {
    // Members
    pub static ref MEMBER_0: Address = "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        .parse()
        .unwrap();
    pub static ref MEMBER_1: Address = "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ"
        .parse()
        .unwrap();
    pub static ref MEMBER_2: Address = "5G6eTvyrydaA6fFuBVnr3A5VAzAMu7NeKKfTZ2NEWirQGCk7".parse().unwrap();
    pub static ref MEMBER_3: Address = "5Cqm7KKdm8MB7jR66mxKKcUAzKGFbZnYJqMvQQBpgC5P9C2W".parse().unwrap();

    // Validators
    pub static ref VALIDATOR_0: Address = "5CMa9MstDT8rndDwFtcAx19YxhVv8j4uE273xkLssq5hjjj1".parse().unwrap();
    pub static ref VALIDATOR_1: Address = "5DfhGyQdFobKM8NsWvEeAKk5EQQgYe9AydgJ7rMB6E1EqRzV".parse().unwrap();

    // Unknown Participant
    pub static ref UNKNOWN_ADDRESS: Address = "5CAjqPPucsE33CbqteYzjmYqLR4FZ97Sjaj2EtzKjtCW2dY3".parse().unwrap();
}

pub fn gen_proposal_with_voters() -> Proposal {
    let mut proposal = gen_proposal();
    proposal.votes.insert(MEMBER_0.clone(), true);
    proposal.votes.insert(MEMBER_1.clone(), true);
    proposal.votes.insert(MEMBER_2.clone(), true);
    proposal.votes.insert(MEMBER_3.clone(), false);

    proposal.votes.insert(VALIDATOR_0.clone(), true);
    proposal.votes.insert(VALIDATOR_1.clone(), true);

    proposal
}

pub fn gen_proposal() -> Proposal {
    let prop_id = 99;
    let new_ap: Address = "5Cqm7KKdm8MB7jR66mxKKcUAzKGFbZnYJqMvQQBpgC5P9C2W"
        .parse()
        .unwrap();
    let proposer: Address = "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        .parse()
        .unwrap();
    Proposal {
        id: prop_id,
        status: ProposalStage::Proposed,
        proposer,
        expiration_block_id: 1000,
        votes: Default::default(),
        proposed_action: AdministrativeTransaction::RegisterAccountAsMember(
            RegisterAccountAsMember {
                address: new_ap,
                encryption_key: [0u8; 32].into(),
            },
        ),
    }
}
