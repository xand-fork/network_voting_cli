use crate::display::models::address::AddressViewModel;

#[test]
fn constructs_full_and_truncated_values() {
    let addr = "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ";

    let vm = AddressViewModel::from(addr.to_string());

    assert_eq!(vm.full_address, addr.to_string());
    assert_eq!(vm.truncated_address, "5EgqcJ1...".to_string());
}

#[test]
fn truncates_multibyte_chars_reasonably() {
    let addr = "你好吗你好吗你好吗你好吗你好吗你好吗‍";

    let vm = AddressViewModel::from(addr.to_string());

    assert_eq!(vm.truncated_address, "你好吗...".to_string());
}
