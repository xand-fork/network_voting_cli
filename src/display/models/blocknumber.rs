use crate::cli::models::blockstamp::Blockstamp;

#[derive(Clone, Debug)]
pub struct BlockNumberViewModel {
    pub block_number: u64,
    pub is_stale: bool,
}

impl From<Blockstamp> for BlockNumberViewModel {
    fn from(b: Blockstamp) -> Self {
        BlockNumberViewModel {
            block_number: b.block_number,
            is_stale: b.is_stale,
        }
    }
}
