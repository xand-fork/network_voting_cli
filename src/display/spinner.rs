use indicatif::ProgressBar;
use indicatif::ProgressStyle;

pub struct Spinner(ProgressBar);

impl Spinner {
    pub fn new(progress_bar_capacity: u64) -> Self {
        let spinner_style = ProgressStyle::default_spinner()
            .tick_chars("⠁⠂⠄⡀⢀⠠⠐⠈ ")
            .template("{spinner}");
        let pb = ProgressBar::new(progress_bar_capacity);
        pb.set_style(spinner_style);
        Spinner(pb)
    }

    /// Increments the spinner by given delta, draws update
    pub fn tick(&mut self, delta: u64) {
        self.0.inc(delta);
    }

    pub fn finish(&mut self) {
        self.0.finish_and_clear();
    }
}
