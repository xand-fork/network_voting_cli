use crate::cli::cli_config::CliConfig;
use crate::display::models::address::AddressViewModel;
use std::fmt::Debug;

#[derive(Clone, Debug)]
pub struct CliConfigViewModel {
    pub issuer: Option<AddressViewModel>,
    pub jwt: Option<String>,
    pub url: Option<String>,
}

impl From<CliConfig> for CliConfigViewModel {
    fn from(c: CliConfig) -> Self {
        CliConfigViewModel {
            issuer: c.issuer.map(AddressViewModel::from),
            jwt: c.jwt,
            url: c.url,
        }
    }
}
