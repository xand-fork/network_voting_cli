use xand_api_client::ProposalStage;

use crate::cli::models::status::Status;

impl From<ProposalStage> for Status {
    fn from(p: ProposalStage) -> Self {
        match p {
            ProposalStage::Proposed => Self::Open,
            ProposalStage::Accepted => Self::Accepted,
            ProposalStage::Rejected => Self::Rejected,
            ProposalStage::Invalid => Self::Invalid,
        }
    }
}
