use crate::cli::interface::vote::Vote;

pub const REJECT_STRING: &str = "Reject";
pub const ACCEPT_STRING: &str = "Accept";

#[derive(Clone, Debug)]
pub struct VoteViewModel {
    pub message: String,
    pub proposal_id: String,
    pub vote_direction: bool,
    pub vote_verb: String,
}

impl From<Vote> for VoteViewModel {
    fn from(v: Vote) -> Self {
        let (proposal_id, vote_direction, vote_verb) = match v {
            Vote::Accept { id } => (id.to_string(), true, ACCEPT_STRING.to_string()),
            Vote::Reject { id } => (id.to_string(), false, REJECT_STRING.to_string()),
        };
        VoteViewModel {
            message: v.to_string(),
            proposal_id,
            vote_direction,
            vote_verb,
        }
    }
}
