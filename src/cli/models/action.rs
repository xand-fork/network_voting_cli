use crate::cli::interface::cli_action::CliAction;

#[derive(Clone, Debug)]
pub enum Action {
    /// Add a new member
    AddMember {
        address: String,
        encryption_key: String,
    },
    /// Remove an existing member
    RemoveMember { address: String },
    /// Change the Trustee for network
    SetTrust {
        address: String,
        encryption_key: String,
    },
    /// Change the global Limited Agent, or remove them altogether
    SetLimitedAgent { address: Option<String> },
    /// Add a new validator
    AddValidator { address: String },
    ///Remove a validator
    RemoveValidator { address: String },
    /// Allowlist a CIDR block
    AllowlistCidrBlock { cidr_block: String, address: String },
    /// Remove a CIDR block from the allowlist
    RemoveAllowlist { cidr_block: String, address: String },
    /// Update Validator code
    SetCode { code: Vec<u8> },
    // temporarily disabled for ADO 6984

    // /// Set rate that Validators are paid
    // SetValidatorEmissionRate {
    //     minor_units_per_emission: u64,
    //     block_quota: u32,
    // },
}

impl From<CliAction> for Action {
    fn from(c: CliAction) -> Self {
        match c {
            CliAction::AddMember {
                address,
                encryption_key,
            } => Action::AddMember {
                address,
                encryption_key,
            },
            CliAction::RemoveMember { address } => Action::RemoveMember { address },
            CliAction::SetTrust {
                address,
                encryption_key,
            } => Action::SetTrust {
                address,
                encryption_key,
            },
            CliAction::SetLimitedAgent { address } => Action::SetLimitedAgent { address },
            CliAction::AddValidator { address } => Action::AddValidator { address },
            CliAction::RemoveValidator { address } => Action::RemoveValidator { address },
            CliAction::Allowlist {
                cidr_block,
                address,
            } => Action::AllowlistCidrBlock {
                cidr_block,
                address,
            },
            CliAction::RemoveAllowlist {
                cidr_block,
                address,
            } => Action::RemoveAllowlist {
                cidr_block,
                address,
            },
            // temporarily disabled for ADO 6984

            // CliAction::SetValidatorEmissionRate {
            //     minor_units_per_block,
            // } => Action::SetValidatorEmissionRate {
            //     block_quota: 1,
            //     minor_units_per_emission: minor_units_per_block,
            // },
        }
    }
}
