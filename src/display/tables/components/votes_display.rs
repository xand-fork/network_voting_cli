use crate::display::cli_display::PrintableTable;
use crate::display::models::votes::VotesViewModel;
use prettytable::{format, Cell, Row, Table};

pub struct VotesDisplay {
    pub votes: VotesViewModel,
}

impl VotesDisplay {
    pub fn new(votes: VotesViewModel) -> Self {
        Self { votes }
    }
}

impl PrintableTable for VotesDisplay {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_NO_BORDER_LINE_SEPARATOR);
        table.add_row(Row::new(vec![
            Cell::new(&format!("{} +", self.votes.yea_voters.count)),
            Cell::new(&format!("{} -", self.votes.nay_voters.count)),
        ]));
        table
    }
}
