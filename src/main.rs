#![forbid(unsafe_code)]

use structopt::StructOpt;

use network_voting_cli::cli::client::xand_api_client_adapter::XandApiClientAdapterFactory;
use network_voting_cli::cli::{handler::Handler, interface::opt::Opt};
use network_voting_cli::display::cli_display::CliDisplay;
use network_voting_cli::display::tables::views::error_result::{
    ErrorResultTable, ErrorResultTableProps,
};

#[tokio::main]
async fn main() {
    let opt = Opt::from_args();
    let client_factory = XandApiClientAdapterFactory;
    let handler = Handler::new(client_factory);
    match handler.await.perform_command(opt.clone()).await {
        Ok(table) => CliDisplay::print_table(table),
        Err(e) => {
            CliDisplay::print_cli_error(&e);
            let error_result = ErrorResultTable::new(ErrorResultTableProps { error: e });
            CliDisplay::print_table(Box::new(error_result));
        }
    }
}
