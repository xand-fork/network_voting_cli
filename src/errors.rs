use std::io;

use snafu::Snafu;
use xand_address::AddressError;
use xand_api_client::errors::XandApiClientError;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[derive(Debug, Snafu)]
#[snafu(visibility(pub))]
pub enum Error {
    #[snafu(display("{}: {}", message, source))]
    XandApiError {
        message: String,
        source: XandApiClientError,
    },
    #[snafu(display("{}", message))]
    InternalError { message: String },
    #[snafu(display("{}: {}", message, source))]
    IoError { message: String, source: io::Error },
    #[snafu(display("{}", message))]
    ConversionError { message: String },
    #[snafu(display("{}", message))]
    CliConfigError { message: String },
    #[snafu(display("Could not parse provided address: {:?}", source))]
    #[snafu(context(false))]
    CliInvalidAddress { source: AddressError },
    #[snafu(display("Could not parse provided cidr block: {:?}", msg))]
    CliInvalidCidrBlock { msg: String },
    #[snafu(display("{}: {}", message, source))]
    ConfigError {
        message: String,
        source: config::ConfigError,
    },
    #[snafu(display("{}: {}", message, source))]
    ParseError {
        message: String,
        source: url::ParseError,
    },
    #[snafu(display("Failed to parse as X25519 public key"))]
    X25519PublicKeyParseError,
    #[snafu(display("{}: {}", message, source))]
    TomlError {
        message: String,
        source: toml::ser::Error,
    },
    #[snafu(display("No default config set: {}", message))]
    NoDefaultConfig { message: String },
}
