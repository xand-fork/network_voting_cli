# Use default tpfs-ci rust pipeline for this repo
include:
  - local: '.ci/tpfs.yaml'

build:
  extends: .build-template

test:
  extends: .test-template

lint:
  extends: .lint-template

audit:
  extends: .audit-template
  script:
    - trace-cmd.sh cargo-audit cargo audit

################################################################################
# Publish docker images
# ---------------------
#
# Arguably these jobs should not live here, but they were added out of
# expediency leading up to the first XAND Simulation Game Day. Saqib
# made a case for migrating it to xand-k8s:
#
# https://gitlab.com/TransparentIncDevelopment/product/apps/network_voting_cli/-/merge_requests/11#note_340563303

.crates-ssh-auth: &crates-ssh-auth
  # Setup ssh agent
  - eval `ssh-agent`
  - chmod 0600 $DEPLOY_SSH_KEY
  - ssh-add $DEPLOY_SSH_KEY

.dind:
  tags:
    - kubernetes-runner
  services:
    - docker:18.09-dind
  image: "$TPFS_RUST_CI_IMG"
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://localhost:2375

publish-docker-image-beta:
  stage: publish
  extends:
    - .dind
  before_script:
    - *crates-ssh-auth
  allow_failure: true
  script:
    - cargo build --release
    - source .ci/.env
    - docker build . -t "$GCR_BETA_IMAGE"
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
    - docker push "$GCR_BETA_IMAGE"
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      when: never
    - if: $CI_MERGE_REQUEST_ID
      when: never
    - when: manual

publish-docker-image-release-gcr:
  stage: publish
  extends:
    - .dind
  before_script:
    - *crates-ssh-auth
  script:
    - cargo build --release
    - source .ci/.env
    - docker build . -t "$GCR_IMAGE"
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
    - docker push "$GCR_IMAGE"
  only:
    refs:
      - master@TransparentIncDevelopment/product/apps/network_voting_cli

publish-docker-image-release-artifactory:
  stage: publish
  extends:
    - .dind
  when: manual
  script:
    - source .ci/.env
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
    - docker pull "$GCR_IMAGE"
    - docker tag "$GCR_IMAGE" "$ARTIFACTORY_IMAGE"
    - docker login -u $ARTIFACTORY_USER -p $ARTIFACTORY_PASS "$ARTIFACTORY_REGISTRY"
    - docker push "$ARTIFACTORY_IMAGE"
  only:
    refs:
      - master@TransparentIncDevelopment/product/apps/network_voting_cli

sync-dependencies:
  extends: .synchronize-central-dependency-registry

upgrade-downstream-repos:
  extends: .upgrade-consuming-repos
  variables:
    UPSTREAM_CRATES: network-voting-cli
  before_script:
    - export REPO_VERSION=$(toml get ./Cargo.toml package.version | tr -d \")