use crate::cli::interface::vote::Vote;
use crate::cli::models::action::Action;
use crate::cli::models::blockstamp::Blockstamp;
use crate::cli::models::proposal::ProposalData;
use crate::errors::Result;

pub mod xand_api_client_adapter;

#[cfg(test)]
pub mod test_utils;

#[async_trait::async_trait]
pub trait Client {
    async fn submit_proposal(&mut self, issuer: String, proposed_action: Action) -> Result<()>;
    async fn submit_vote(&mut self, issuer: String, vote: Vote) -> Result<()>;
    async fn get_all_proposals(&self) -> Result<Vec<ProposalData>>;
    async fn get_proposal(&self, id: u32) -> Result<ProposalData>;
    async fn get_current_block(&self) -> Result<Blockstamp>;
}
