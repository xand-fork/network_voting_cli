use structopt::StructOpt;

#[derive(StructOpt, Debug, Clone)]
pub enum CliAction {
    /// Add a new member
    AddMember {
        /// Address of the member to add ('5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ')
        address: String,

        /// X25519 public encryption key as 32 bytes encoded using base58 ('JEKNVnkbo3jma5nREBBJCDoXFVeKkD56V3xKrvRmWxFG')
        encryption_key: String,
    },

    /// Remove an existing member
    RemoveMember {
        /// Address of the member to remove ('5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ')
        address: String,
    },

    /// Change the Trustee for network
    SetTrust {
        /// Address of the trust to set ('5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ')
        address: String,

        /// X25519 public encryption key as 32 bytes encoded using base58 ('JEKNVnkbo3jma5nREBBJCDoXFVeKkD56V3xKrvRmWxFG')
        encryption_key: String,
    },

    /// Set (or Remove) the Limited Agent
    SetLimitedAgent {
        /// Address for the new Limited Agent. Leave unset to indicate removal.
        address: Option<String>,
    },

    /// Add a new validator
    AddValidator {
        /// Address of the validator to add ('5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ')
        address: String,
    },

    /// Remove a validator
    RemoveValidator {
        /// Address of the validator to remove ('5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ')
        address: String,
    },

    /// Allowlist a CIDR block
    Allowlist {
        /// CIDR block of IPv4 addresses to allow for communications ('127.0.0.1/32')
        cidr_block: String,

        /// Address of the network participant using the CIDR block ('5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ')
        address: String,
    },

    /// Remove a CIDR block from the allowlist
    RemoveAllowlist {
        /// CIDR block of IPv4 addresses to disallow for communications ('127.0.0.1/32')
        cidr_block: String,

        /// Address of the network participant using the CIDR block ('5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ')
        address: String,
    },
    // temporarily disabled for ADO 6984
    // /// Set the rate at which Validators are paid
    // SetValidatorEmissionRate {
    //     /// Amount paid, in minor units, per block produced by an individual Validator
    //     minor_units_per_block: u64,
    // },
}
