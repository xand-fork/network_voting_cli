use std::fmt::Display;

use crate::display::cli_display::PrintableTable;

pub mod action_summary;
pub mod address_display;
pub mod block_staleness_indicator;
pub mod colorized_status_display;
pub mod config_summary;
pub mod voters_summary;
pub mod votes_display;

pub trait TableComponent: PrintableTable {}
pub trait StringComponent: Display {}
