use crate::display::cli_display::PrintableTable;
use crate::display::models::action::ActionViewModel;
use crate::display::models::address::AddressViewModel;
use crate::display::models::blocknumber::BlockNumberViewModel;
use crate::display::models::proposal::{ProposalViewModel, ProposalsViewModel};
use crate::display::models::status::StatusViewModel;
use crate::display::models::votes::VotesViewModel;
use crate::display::tables::views::proposal_list::{ProposalListTable, ProposalListTableProps};

fn create_fake_proposal(id: u32) -> ProposalViewModel {
    ProposalViewModel {
        id,
        action: ActionViewModel::Invalid,
        member_votes: VotesViewModel::default(),
        validator_votes: VotesViewModel::default(),
        status: StatusViewModel("Fake Status".to_string()),
        proposer: AddressViewModel {
            full_address: "address!".to_string(),
            truncated_address: "address!".to_string(),
        },
        expires: "".to_string(),
    }
}

#[test]
fn prints_proposals_in_ascending_id_order() {
    let proposal_one = create_fake_proposal(0);
    let proposal_two = create_fake_proposal(1);
    let proposal_three = create_fake_proposal(2);
    let proposals = ProposalsViewModel(vec![proposal_three, proposal_one, proposal_two]); // purposely misordered

    let view = ProposalListTable::new(ProposalListTableProps {
        current_block: BlockNumberViewModel {
            block_number: 0,
            is_stale: false,
        },
        truncate_addresses: false,
        proposals,
    });
    let constructed_table = view.get_pretty_table();
    let first_proposal_row_id = constructed_table
        .get_row(2)
        .unwrap()
        .get_cell(0)
        .unwrap()
        .get_content();
    let second_proposal_row_id = constructed_table
        .get_row(3)
        .unwrap()
        .get_cell(0)
        .unwrap()
        .get_content();
    let third_proposal_row_id = constructed_table
        .get_row(4)
        .unwrap()
        .get_cell(0)
        .unwrap()
        .get_content();

    assert_eq!(first_proposal_row_id, "0".to_string());
    assert_eq!(second_proposal_row_id, "1".to_string());
    assert_eq!(third_proposal_row_id, "2".to_string());
}

#[test]
fn more_than_10_proposals_are_ordered_by_numeric_index() {
    // Given
    // purposely reverse for backwards ordering
    let proposals: Vec<ProposalViewModel> = (0..100).rev().map(create_fake_proposal).collect();
    let proposals_vm = ProposalsViewModel(proposals);

    let view = ProposalListTable::new(ProposalListTableProps {
        current_block: BlockNumberViewModel {
            block_number: 0,
            is_stale: false,
        },
        truncate_addresses: false,
        proposals: proposals_vm,
    });

    // When
    let constructed_table = view.get_pretty_table();

    // Then
    let expected_ordering: Vec<String> = (0..100).map(|i| i.to_string()).collect();
    let mut actual_ordering: Vec<String> = constructed_table
        .column_iter(0)
        .skip(2)
        .map(|c| c.get_content())
        .collect();
    actual_ordering.pop(); // Get rid of "Block: XXX" block number print
    assert_eq!(actual_ordering, expected_ordering);
}
