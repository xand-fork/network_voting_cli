use crate::cli::models::blockstamp::Blockstamp as CLIBlockstamp;
use xand_api_client::Blockstamp;

impl From<Blockstamp> for CLIBlockstamp {
    fn from(other: Blockstamp) -> Self {
        CLIBlockstamp {
            block_number: other.block_number,
            timestamp_ms: other.unix_timestamp_ms,
            is_stale: other.is_stale,
        }
    }
}
