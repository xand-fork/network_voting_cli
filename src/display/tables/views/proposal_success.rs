use prettytable::{format, Row, Table};

use crate::display::cli_display::PrintableTable;
use crate::display::models::action::ActionViewModel;
use crate::display::tables::components::action_summary::{
    ActionSummaryComponent, ActionSummaryComponentProps,
};

pub const TITLE: &str = "Proposal Submission Successful";

pub struct ProposalSuccessTableProps {
    pub action: ActionViewModel,
    pub truncate_addresses: bool,
}

pub struct ProposalSuccessTable {
    props: ProposalSuccessTableProps,
}

impl ProposalSuccessTable {
    pub fn new(props: ProposalSuccessTableProps) -> Self {
        ProposalSuccessTable { props }
    }

    pub fn create_action_row(&self) -> Row {
        let action_component = ActionSummaryComponent::new(ActionSummaryComponentProps {
            action: self.props.action.clone(),
            truncate_addresses: self.props.truncate_addresses,
        });
        row![c=>action_component]
    }
}

impl PrintableTable for ProposalSuccessTable {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_DEFAULT);
        table.add_row(row![bc=>TITLE]);
        table.add_row(self.create_action_row());
        table
    }
}
