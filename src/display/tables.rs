/// Partial Tables and display-able structs for use in the construction of views
pub mod components;
/// Tables that are meant to be displayed as the result of some action
pub mod views;

#[cfg(test)]
mod test_utils;
