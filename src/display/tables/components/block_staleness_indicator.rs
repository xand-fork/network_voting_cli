use std::fmt::{Display, Formatter};

use colored::*;

use crate::display::models::blocknumber::BlockNumberViewModel;
use crate::display::tables::components::StringComponent;

#[cfg(test)]
mod tests;

const STALE_COLOR: Color = Color::Red;

pub struct BlockStalenessIndicatorComponentProps {
    pub block: BlockNumberViewModel,
}

pub struct BlockStalenessIndicatorComponent {
    props: BlockStalenessIndicatorComponentProps,
}

impl BlockStalenessIndicatorComponent {
    pub fn new(props: BlockStalenessIndicatorComponentProps) -> Self {
        BlockStalenessIndicatorComponent { props }
    }
}

impl StringComponent for BlockStalenessIndicatorComponent {}

impl Display for BlockStalenessIndicatorComponent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.props.block.is_stale {
            write!(
                f,
                "Block: {} {}",
                self.props
                    .block
                    .block_number
                    .to_string()
                    .as_str()
                    .color(STALE_COLOR),
                "(Stale)".color(STALE_COLOR)
            )
        } else {
            write!(f, "Block: {}", self.props.block.block_number)
        }
    }
}
