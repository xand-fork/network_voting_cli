use crate::display::models::address::AddressesViewModel;
use crate::display::models::votes::VotesViewModel;

impl Default for VotesViewModel {
    fn default() -> Self {
        Self {
            yea_voters: AddressesViewModel {
                addresses: vec![],
                count: 0,
            },
            nay_voters: AddressesViewModel {
                addresses: vec![],
                count: 0,
            },
        }
    }
}
