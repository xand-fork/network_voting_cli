use crate::cli::client::test_utils::*;
use crate::cli::client::xand_api_client_adapter::data_model_translations::votes::*;

pub fn get_participants() -> Participants {
    Participants {
        members: vec![
            MEMBER_0.clone(),
            MEMBER_1.clone(),
            MEMBER_2.clone(),
            MEMBER_3.clone(),
        ],
        validators: vec![VALIDATOR_0.clone(), VALIDATOR_1.clone()],
    }
}

#[test]
fn sorts_member_votes() {
    let prop = gen_proposal_with_voters();
    let voters = build_participant_votes(&prop, get_participants()).unwrap();

    assert_eq!(voters.members.yea_voters.len(), 3);
    assert_eq!(voters.members.nay_voters.len(), 1);
}

#[test]
fn sorts_validator_votes() {
    let prop = gen_proposal_with_voters();
    let voters = build_participant_votes(&prop, get_participants()).unwrap();

    assert_eq!(voters.validators.yea_voters.len(), 2);
    assert_eq!(voters.validators.nay_voters.len(), 0);
}

#[test]
fn unknown_voter_returns_error() {
    let mut prop = gen_proposal_with_voters();
    prop.votes.insert(UNKNOWN_ADDRESS.clone(), false);
    let result = build_participant_votes(&prop, get_participants());

    assert!(matches!(result, Err(Error::InternalError { .. })));
}

#[test]
fn prop_with_no_votes_returns_empty_voters() {
    let prop = gen_proposal();
    let voters = build_participant_votes(&prop, get_participants()).unwrap();

    assert!(voters.validators.yea_voters.is_empty());
    assert!(voters.validators.nay_voters.is_empty());

    assert!(voters.members.yea_voters.is_empty());
    assert!(voters.members.nay_voters.is_empty());
}
