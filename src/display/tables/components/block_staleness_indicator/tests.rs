use crate::display::models::blocknumber::BlockNumberViewModel;
use crate::display::tables::components::block_staleness_indicator::{
    BlockStalenessIndicatorComponent, BlockStalenessIndicatorComponentProps,
};

#[test]
fn shows_staleness_warning() {
    let component = BlockStalenessIndicatorComponent::new(BlockStalenessIndicatorComponentProps {
        block: BlockNumberViewModel {
            block_number: 9001,
            is_stale: true,
        },
    });

    let component_string = component.to_string();

    assert!(component_string.contains("Stale"));
}

#[test]
fn shows_block_number_without_warning() {
    let component = BlockStalenessIndicatorComponent::new(BlockStalenessIndicatorComponentProps {
        block: BlockNumberViewModel {
            block_number: 9001,
            is_stale: false,
        },
    });

    let component_string = component.to_string();

    assert!(!component_string.contains("Stale"));
    assert!(component_string.contains("9001"));
}
