#[cfg(test)]
mod tests;

use crate::cli::models::votes::VotesData;
use crate::errors::{Error, Result};
use xand_address::Address;
use xand_api_client::Proposal;

pub struct Participants {
    pub members: Vec<Address>,
    pub validators: Vec<Address>,
}

pub struct ParticipantVotes {
    pub members: VotesData,
    pub validators: VotesData,
}

pub fn build_participant_votes(
    prop: &Proposal,
    participants: Participants,
) -> Result<ParticipantVotes> {
    let mut member_votes = VotesData::new();
    let mut validator_votes = VotesData::new();

    for (voter, vote) in prop.votes.clone() {
        if participants.members.contains(&voter) {
            member_votes.record_vote(voter.to_string(), vote);
        } else if participants.validators.contains(&voter) {
            validator_votes.record_vote(voter.to_string(), vote);
        } else {
            return Err(Error::InternalError {
                message: "Consistency error encountered while calculating votes. Please try again."
                    .to_string(),
            });
        }
    }

    Ok(ParticipantVotes {
        members: member_votes,
        validators: validator_votes,
    })
}
