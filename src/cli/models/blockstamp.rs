#[derive(Clone, Debug)]
pub struct Blockstamp {
    pub block_number: u64,
    pub timestamp_ms: i64,
    pub is_stale: bool,
}
