use std::convert::TryInto;

use xand_api_client::{
    AddAuthorityKey, AdministrativeTransaction, CidrBlock, RegisterAccountAsMember,
    RemoveAuthorityKey, RemoveMember, RootAllowlistCidrBlock, RootRemoveAllowlistCidrBlock,
    SetLimitedAgentId, SetTrustNodeId, /* SetValidatorEmissionRate, */
};

use crate::cli::client::test_utils::MEMBER_0;
use crate::cli::interface::cli_action::CliAction;
use crate::cli::models::action::Action;

const CIDR_BLOCK: CidrBlock = CidrBlock([192, 168, 0, 0, 24]);

#[test]
fn propose_adding_a_member() {
    let action = CliAction::AddMember {
        address: MEMBER_0.to_string(),
        encryption_key: bs58::encode([0xEFu8; 32]).into_string(),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::RegisterAccountAsMember(RegisterAccountAsMember {
            address,
            encryption_key,
        }) if address == *MEMBER_0 && encryption_key == [0xEFu8; 32].into()
    ))
}

#[test]
fn propose_adding_a_validator() {
    let action = CliAction::AddValidator {
        address: MEMBER_0.to_string(),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::AddAuthorityKey(AddAuthorityKey {
            account_id,
        }) if account_id == *MEMBER_0
    ))
}

#[test]
fn propose_removing_a_member() {
    let action = CliAction::RemoveMember {
        address: MEMBER_0.to_string(),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::RemoveMember(RemoveMember {
            address,
        }) if address == *MEMBER_0
    ))
}

#[test]
fn propose_removing_a_validator() {
    let action = CliAction::RemoveValidator {
        address: MEMBER_0.to_string(),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::RemoveAuthorityKey(RemoveAuthorityKey {
            account_id,
        }) if account_id == *MEMBER_0
    ))
}

#[test]
fn propose_removing_allowlist() {
    let action = CliAction::RemoveAllowlist {
        cidr_block: CIDR_BLOCK.to_string(),
        address: MEMBER_0.to_string(),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::RootRemoveAllowlistCidrBlock(
            RootRemoveAllowlistCidrBlock {
                cidr_block,
                account,
            },
        ) if cidr_block == CIDR_BLOCK && account == *MEMBER_0
    ))
}

//  NOTE: There is no CLI action for set code yet
// #[tokio::test]
// async fn propose_set_code() {
//     const CODE: [u8; 3] = [0, 1, 2];
//     let action = CLIAction::SetCode {
//         code: CODE.to_vec(),
//     };
//     proposal_can_be_submitted(action, |transaction| {
//         matches!(
//             transaction,
//             AdministrativeTransaction::SetCode(SetCode {
//                 code,
//             }) if *code == CODE
//         )
//     })
//     .await;
// }

#[test]
fn propose_set_trust() {
    let action = CliAction::SetTrust {
        address: MEMBER_0.to_string(),
        encryption_key: bs58::encode([0x01u8; 32]).into_string(),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::SetTrust(SetTrustNodeId {
            address,
            encryption_key,
        }) if address == *MEMBER_0 && encryption_key == [0x01u8; 32].into()
    ))
}

#[test]
fn propose_set_limited_agent() {
    let action = CliAction::SetLimitedAgent {
        address: Some(MEMBER_0.to_string()),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::SetLimitedAgent(SetLimitedAgentId {
            address: Some(address_str),
        }) if address_str == *MEMBER_0
    ))
}

#[test]
fn propose_allowlist() {
    let action = CliAction::Allowlist {
        cidr_block: CIDR_BLOCK.to_string(),
        address: MEMBER_0.to_string(),
    };
    let txn: Action = action.try_into().unwrap();
    let txn: AdministrativeTransaction = txn.try_into().unwrap();
    assert!(matches!(
        txn,
        AdministrativeTransaction::RootAllowlistCidrBlock(RootAllowlistCidrBlock {
            cidr_block,
            account,
        }) if cidr_block == CIDR_BLOCK && account == *MEMBER_0
    ))
}

// temporarily disabled for ADO 6984

// #[test]
// fn propose_set_validator_emission_rate() {
//     let action = CliAction::SetValidatorEmissionRate {
//         minor_units_per_block: 17,
//     };
//     let txn: Action = action.try_into().unwrap();
//     let txn: AdministrativeTransaction = txn.try_into().unwrap();
//     assert!(matches!(
//         txn,
//         AdministrativeTransaction::SetValidatorEmissionRate(SetValidatorEmissionRate {
//             block_quota: 1,
//             minor_units_per_emission: 17,
//         })
//     ))
// }
