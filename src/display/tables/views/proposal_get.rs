use prettytable::format::Alignment;
use prettytable::{format, Attr, Cell, Row, Table};

use crate::display::cli_display::PrintableTable;
use crate::display::models::blocknumber::BlockNumberViewModel;
use crate::display::models::proposal::ProposalViewModel;
use crate::display::tables::components::action_summary::{
    ActionSummaryComponent, ActionSummaryComponentProps,
};
use crate::display::tables::components::address_display::{
    AddressDisplayComponent, AddressDisplayComponentProps,
};
use crate::display::tables::components::block_staleness_indicator::{
    BlockStalenessIndicatorComponent, BlockStalenessIndicatorComponentProps,
};
use crate::display::tables::components::colorized_status_display::ColorizedStatusComponent;
use crate::display::tables::components::voters_summary::{
    VotesSummaryComponent, VotesSummaryComponentProps,
};

pub const TITLE: &str = "Proposal";
pub const NO_NAY_VOTERS_MESSAGE: &str = "No Nay votes";
pub const NO_YEA_VOTERS_MESSAGE: &str = "No Yea votes";
pub const TABLE_WIDTH: usize = 2;

pub struct ProposalTableProps {
    pub current_block: BlockNumberViewModel,
    pub proposal: ProposalViewModel,
    pub truncate_addresses: bool,
}

pub struct ProposalTable {
    props: ProposalTableProps,
}

impl ProposalTable {
    pub fn new(props: ProposalTableProps) -> Self {
        ProposalTable { props }
    }

    fn create_action_row(&self) -> Row {
        let action_component = ActionSummaryComponent::new(ActionSummaryComponentProps {
            action: self.props.proposal.action.clone(),
            truncate_addresses: self.props.truncate_addresses,
        });
        Row::new(vec![
            cell!("Action").with_style(Attr::Bold),
            cell!(action_component),
        ])
    }

    fn create_proposer_row(&self) -> Row {
        let proposer_component = AddressDisplayComponent::new(AddressDisplayComponentProps {
            truncate: self.props.truncate_addresses,
            address: self.props.proposal.proposer.clone(),
            string_color: None,
        });
        Row::new(vec![
            cell!("Proposer").with_style(Attr::Bold),
            cell!(proposer_component),
        ])
    }

    fn create_block_staleness_row(&self) -> Row {
        let staleness_indicator =
            BlockStalenessIndicatorComponent::new(BlockStalenessIndicatorComponentProps {
                block: self.props.current_block.clone(),
            });
        Row::new(vec![cell!(staleness_indicator).with_hspan(TABLE_WIDTH)])
    }

    fn create_title_row(&self) -> Row {
        let title = Cell::new_align(TITLE, Alignment::CENTER)
            .with_hspan(TABLE_WIDTH)
            .with_style(Attr::Bold);
        Row::new(vec![title])
    }
}

impl PrintableTable for ProposalTable {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_DEFAULT);
        table.add_row(self.create_title_row());
        table.add_row(Row::new(vec![
            cell!("Id").with_style(Attr::Bold),
            cell!(self.props.proposal.id),
        ]));
        table.add_row(self.create_proposer_row());
        table.add_row(self.create_action_row());
        table.add_row(
            row![b->"Status", ColorizedStatusComponent { status: self.props.proposal.status.clone() } ],
        );
        table.add_row(row![b->"Expires", self.props.proposal.expires]);
        table.add_row(
            row![b->"Member Votes", VotesSummaryComponent::new(VotesSummaryComponentProps {
        votes: self.props.proposal.member_votes.clone(),
        truncate_addresses: self.props.truncate_addresses,
        }).get_pretty_table()],
        );
        table.add_row(
            row![b->"Validator Votes", VotesSummaryComponent::new(VotesSummaryComponentProps {
            votes: self.props.proposal.validator_votes.clone(),
            truncate_addresses: self.props.truncate_addresses,
            }).get_pretty_table()],
        );
        table.add_row(self.create_block_staleness_row());
        table
    }
}
