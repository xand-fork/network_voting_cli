use prettytable::format::Alignment;
use prettytable::{color as pt_color, format, Attr, Cell, Row, Table};

use crate::display::cli_display::PrintableTable;
use crate::display::models::blocknumber::BlockNumberViewModel;
use crate::display::models::proposal::{ProposalViewModel, ProposalsViewModel};
use crate::display::tables::components::action_summary::{
    ActionSummaryComponent, ActionSummaryComponentProps,
};
use crate::display::tables::components::address_display::{
    AddressDisplayComponent, AddressDisplayComponentProps,
};
use crate::display::tables::components::block_staleness_indicator::{
    BlockStalenessIndicatorComponent, BlockStalenessIndicatorComponentProps,
};
use crate::display::tables::components::colorized_status_display::ColorizedStatusComponent;
use crate::display::tables::components::votes_display::VotesDisplay;

#[cfg(test)]
mod tests;

pub const NO_PROPOSALS_MESSAGE: &str = "No proposals found";
pub const TITLE: &str = "All Proposals";
pub const TABLE_WIDTH: usize = 7;

pub struct ProposalListTableProps {
    pub current_block: BlockNumberViewModel,
    pub truncate_addresses: bool,
    pub proposals: ProposalsViewModel,
}

pub struct ProposalListTable {
    props: ProposalListTableProps,
}

impl ProposalListTable {
    pub fn new(props: ProposalListTableProps) -> Self {
        ProposalListTable { props }
    }

    fn create_title_row(&self) -> Row {
        let title_cell = Cell::new_align(TITLE, Alignment::CENTER)
            .with_hspan(TABLE_WIDTH)
            .with_style(Attr::Bold);
        Row::new(vec![title_cell])
    }

    fn create_header_row(&self) -> Row {
        let id_column_header = Cell::new("ID").with_style(Attr::Bold);
        let action_column_header = Cell::new("Action").with_style(Attr::Bold);
        let member_votes_column_header = Cell::new("Member Votes").with_style(Attr::Bold);
        let validator_votes_column_header = Cell::new("Validator Votes").with_style(Attr::Bold);
        let status_column_header = Cell::new("Status").with_style(Attr::Bold);
        let proposer_column_header = Cell::new("Proposer").with_style(Attr::Bold);
        let expiration_block_column_header = Cell::new("Expiration Block").with_style(Attr::Bold);
        Row::new(vec![
            id_column_header,
            action_column_header,
            member_votes_column_header,
            validator_votes_column_header,
            status_column_header,
            proposer_column_header,
            expiration_block_column_header,
        ])
    }

    fn order_proposals_by_id(&self) -> Vec<ProposalViewModel> {
        let mut proposals = self.props.proposals.0.clone();
        proposals.sort_by(|a, b| a.id.cmp(&b.id));
        proposals
    }

    fn create_proposal_row(&self, proposal: &ProposalViewModel) -> Row {
        row![
            proposal.id,
            ActionSummaryComponent::new(ActionSummaryComponentProps {
                truncate_addresses: self.props.truncate_addresses,
                action: proposal.action.clone(),
            }),
            c->VotesDisplay::new(proposal.member_votes.clone()).get_pretty_table(),
            c->VotesDisplay::new(proposal.validator_votes.clone()).get_pretty_table(),
            ColorizedStatusComponent {
                status: proposal.status.clone()
            },
            AddressDisplayComponent::new(AddressDisplayComponentProps {
                truncate: self.props.truncate_addresses,
                address: proposal.proposer.clone(),
                string_color: None,
            }),
            proposal.expires
        ]
    }

    fn create_no_proposals_row(&self) -> Row {
        let no_proposals_msg_cell = Cell::new_align(NO_PROPOSALS_MESSAGE, Alignment::CENTER)
            .with_hspan(TABLE_WIDTH)
            .with_style(Attr::Bold)
            .with_style(Attr::ForegroundColor(pt_color::RED));
        Row::new(vec![no_proposals_msg_cell])
    }

    fn create_block_staleness_row(&self) -> Row {
        let staleness_indicator =
            BlockStalenessIndicatorComponent::new(BlockStalenessIndicatorComponentProps {
                block: self.props.current_block.clone(),
            });
        Row::new(vec![cell!(staleness_indicator).with_hspan(TABLE_WIDTH)])
    }
}

impl PrintableTable for ProposalListTable {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_DEFAULT);
        table.add_row(self.create_title_row());
        table.add_row(self.create_header_row());

        if self.props.proposals.0.is_empty() {
            table.add_row(self.create_no_proposals_row());
            table.add_row(self.create_block_staleness_row());
            return table;
        };

        let ordered_proposals = self.order_proposals_by_id();
        for prop in ordered_proposals.iter() {
            let proposal_row = self.create_proposal_row(prop);
            table.add_row(proposal_row);
        }
        table.add_row(self.create_block_staleness_row());
        table
    }
}
