use std::fmt::{Display, Formatter, Result as FmtResult};

use structopt::StructOpt;

use xand_api_client::VoteProposal;

#[derive(StructOpt, Debug, Copy, Clone)]
pub enum Vote {
    /// Vote to accept proposal
    #[structopt(name = "accept")]
    Accept {
        /// Proposal id to accept
        id: u32,
    },
    /// Vote to reject proposal
    #[structopt(name = "reject")]
    Reject {
        /// Proposal id to reject
        id: u32,
    },
}

impl From<Vote> for VoteProposal {
    fn from(vote: Vote) -> Self {
        match vote {
            Vote::Accept { id } => VoteProposal { id, vote: true },
            Vote::Reject { id } => VoteProposal { id, vote: false },
        }
    }
}

impl Display for Vote {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Vote::Accept { id } => write!(f, "Accept Proposal {}", id),
            Vote::Reject { id } => write!(f, "Reject Proposal {}", id),
        }
    }
}
