use crate::cli::interface::opt::{ConfigOpts, FormattingOpts};
use crate::display::cli_display::PrintableTable;
use crate::display::models::config::CliConfigViewModel;
use crate::display::tables::views::config_get::{ConfigTable, ConfigTableProps};
use crate::display::tables::views::config_set::{ConfigSetTable, ConfigSetTableProps};
use crate::display::tables::views::proposal_get::{ProposalTable, ProposalTableProps};
use crate::display::tables::views::proposal_list::{ProposalListTable, ProposalListTableProps};
use crate::display::tables::views::proposal_success::{
    ProposalSuccessTable, ProposalSuccessTableProps,
};
use crate::display::tables::views::simple_message::{SimpleMessageTable, SimpleMessageTableProps};
use crate::{
    cli::cli_config::{
        get_default_config, maybe_override_or_default_jwt, override_or_default_issuer,
        override_or_default_url, save_config_values,
    },
    cli::client::Client,
    cli::interface::{opt::Opt, vote::Vote},
    cli::models::action::Action,
    errors::Result,
};

#[async_trait::async_trait]
pub trait ClientFactory {
    type NewClient: Client;

    async fn new_client(&self, url: String, maybe_jwt: Option<String>) -> Result<Self::NewClient>;
}

pub struct Handler<T: ClientFactory> {
    client_factory: T,
}

impl<T: ClientFactory> Handler<T> {
    pub async fn new(client_factory: T) -> Self {
        Handler { client_factory }
    }

    async fn new_client(&self, url: String, maybe_jwt: Option<String>) -> Result<T::NewClient> {
        self.client_factory.new_client(url, maybe_jwt).await
    }

    pub async fn perform_command(&mut self, opt: Opt) -> Result<Box<dyn PrintableTable>> {
        let result_table = match opt {
            Opt::List {
                config_override,
                formatting,
            } => {
                self.print_all_proposals(config_override, formatting)
                    .await?
            }
            Opt::GetProposal {
                id,
                config_override,
                formatting,
            } => self.print_proposal(id, config_override, formatting).await?,
            Opt::Propose {
                action,
                config_override,
                formatting,
            } => {
                self.propose_action(action.into(), config_override, formatting)
                    .await?
            }
            Opt::Vote {
                vote,
                config_override,
            } => self.print_vote_result(vote, config_override).await?,
            Opt::SetConfig {
                new_values,
                formatting,
            } => self.set_config(new_values, formatting).await?,
            Opt::GetConfig { formatting } => Self::print_config(formatting).await?,
        };
        Ok(result_table)
    }

    async fn set_config(
        &self,
        new_values: ConfigOpts,
        formatting: FormattingOpts,
    ) -> Result<Box<dyn PrintableTable>> {
        if new_values.url.is_none() && new_values.issuer.is_none() && new_values.jwt.is_none() {
            let message_result = SimpleMessageTable::new(SimpleMessageTableProps {
                title: "Configuration Not Changed; No Values Given".to_string(),
                message: "Please specify values to change. Pass `set_config -h` to learn more"
                    .to_string(),
            });
            Ok(Box::new(message_result))
        } else {
            let possible_old_cfg = get_default_config();
            let possible_old_cfg = match possible_old_cfg {
                Ok(c) => Some(CliConfigViewModel::from(c)),
                Err(_) => None,
            };
            let cfg = save_config_values(new_values.url, new_values.issuer, new_values.jwt)?;
            let config_set_table = ConfigSetTable::new(ConfigSetTableProps {
                old_config: possible_old_cfg,
                new_config: cfg.into(),
                truncate_addresses: !formatting.no_truncate,
            });
            Ok(Box::new(config_set_table))
        }
    }

    async fn print_config(formatting: FormattingOpts) -> Result<Box<dyn PrintableTable>> {
        let cfg = get_default_config()?;
        let config_table = ConfigTable::new(ConfigTableProps {
            config: cfg.into(),
            truncate_addresses: !formatting.no_truncate,
        });
        Ok(Box::new(config_table))
    }

    async fn print_all_proposals(
        &self,
        config_override: ConfigOpts,
        formatting: FormattingOpts,
    ) -> Result<Box<dyn PrintableTable>> {
        let url = override_or_default_url(config_override.url)?;
        let maybe_jwt = maybe_override_or_default_jwt(config_override.jwt)?;
        let client = self.new_client(url, maybe_jwt).await?;
        let all_proposals = client.get_all_proposals().await?;
        // TODO: remove secondary call https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Governance/Features/?workitem=5579
        let current_block = client.get_current_block().await?;
        let proposals_list = ProposalListTable::new(ProposalListTableProps {
            current_block: current_block.into(),
            proposals: all_proposals.into(),
            truncate_addresses: !formatting.no_truncate,
        });
        Ok(Box::new(proposals_list))
    }

    async fn print_proposal(
        &self,
        id: u32,
        config_override: ConfigOpts,
        formatting: FormattingOpts,
    ) -> Result<Box<dyn PrintableTable>> {
        let url = override_or_default_url(config_override.url)?;
        let maybe_jwt = maybe_override_or_default_jwt(config_override.jwt)?;
        let client = self.new_client(url, maybe_jwt).await?;
        let proposal = client.get_proposal(id).await?;
        // TODO: remove secondary call https://dev.azure.com/transparentsystems/xand-network/_backlogs/backlog/Governance/Features/?workitem=5579
        let current_block = client.get_current_block().await?;
        let proposal_view = ProposalTable::new(ProposalTableProps {
            current_block: current_block.into(),
            proposal: proposal.into(),
            truncate_addresses: !formatting.no_truncate,
        });
        Ok(Box::new(proposal_view))
    }

    async fn propose_action(
        &mut self,
        action: Action,
        config_override: ConfigOpts,
        formatting: FormattingOpts,
    ) -> Result<Box<dyn PrintableTable>> {
        let url = override_or_default_url(config_override.url)?;
        let issuer = override_or_default_issuer(config_override.issuer)?;
        let maybe_jwt = maybe_override_or_default_jwt(config_override.jwt)?;
        self.new_client(url, maybe_jwt)
            .await?
            .submit_proposal(issuer, action.clone())
            .await?;
        let proposal_submission_result = ProposalSuccessTable::new(ProposalSuccessTableProps {
            action: action.into(),
            truncate_addresses: !formatting.no_truncate,
        });
        Ok(Box::new(proposal_submission_result))
    }

    async fn print_vote_result(
        &mut self,
        vote: Vote,
        config_override: ConfigOpts,
    ) -> Result<Box<dyn PrintableTable>> {
        let url = override_or_default_url(config_override.url)?;
        let issuer = override_or_default_issuer(config_override.issuer)?;
        let maybe_jwt = maybe_override_or_default_jwt(config_override.jwt)?;
        self.new_client(url, maybe_jwt)
            .await?
            .submit_vote(issuer, vote)
            .await?;
        let message_result = SimpleMessageTable::new(SimpleMessageTableProps {
            title: "Vote Successful".to_string(),
            message: format!("Submitted vote to {}", vote),
        });
        Ok(Box::new(message_result))
    }
}
