FROM debian:stable

RUN apt-get update -yqq
RUN apt-get install -yqq curl ca-certificates

COPY target/release/network-voting-cli /usr/bin
ENTRYPOINT ["network-voting-cli"]
