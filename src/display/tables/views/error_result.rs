use prettytable::{format, Table};

use crate::display::cli_display::PrintableTable;
use crate::errors::Error;

pub const TITLE: &str = "Error Result";
pub const LABEL_TEXT: &str = "The Network Voting CLI action resulted in the following error:\n";

pub struct ErrorResultTableProps {
    pub error: Error,
}

pub struct ErrorResultTable {
    props: ErrorResultTableProps,
}

impl ErrorResultTable {
    pub fn new(props: ErrorResultTableProps) -> Self {
        ErrorResultTable { props }
    }
}

impl PrintableTable for ErrorResultTable {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_CLEAN);
        table.add_row(row![blFr=>TITLE]);
        let message = format!("{}{:?}", LABEL_TEXT, self.props.error);
        table.add_row(row![message]);
        table
    }
}
