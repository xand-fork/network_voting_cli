use std::time::SystemTime;

use chrono::DateTime;
use pseudo::Mock;
use xand_address::Address;
use xand_api_client::errors::XandApiClientError;
use xand_api_client::models::TransactionUpdate;
use xand_api_client::{
    models::{Paginated, Paging},
    AdministrativeTransaction, Blockstamp, CidrBlock, PendingCreateRequest, PendingRedeemRequest,
    Proposal, RegisterAccountAsMember, Transaction, TransactionFilter, TransactionHistoryPage,
    TransactionId, TransactionStatus, TransactionStatusStream, VoteProposal, XandApiClientTrait,
    XandTransaction,
};

use crate::cli::client::test_utils::{gen_proposal, *};
use crate::cli::client::xand_api_client_adapter::XandApiClientAdapter;
use crate::cli::client::Client;
use crate::cli::interface::vote::Vote;

type TxUpdateStream =
    Result<Vec<Result<TransactionUpdate, XandApiClientError>>, XandApiClientError>;
#[derive(Clone)]
pub struct MockXandApiClient {
    pub propose_action: Mock<(Address, AdministrativeTransaction), TxUpdateStream>,
    pub vote_on_proposal: Mock<(Address, VoteProposal), TxUpdateStream>,
    pub get_all_proposals: Mock<(), Vec<Proposal>>,
    pub txn_details: Mock<TransactionId, Result<Transaction, XandApiClientError>>,
}

impl MockXandApiClient {
    fn from_proposals(proposals: Vec<Proposal>) -> Self {
        Self {
            propose_action: Mock::new(Ok(Default::default())),
            vote_on_proposal: Mock::new(Ok(Default::default())),
            get_all_proposals: Mock::new(proposals),
            txn_details: Mock::new(Err(XandApiClientError::Timeout)),
        }
    }
}

// The xand api client trait needs to be finer grained. That isn't done yet, so
// we mock out this big trait, only implementing the bits we need.
#[async_trait::async_trait]
#[allow(unused_variables)]
impl XandApiClientTrait for MockXandApiClient {
    async fn submit_transaction(
        &self,
        issuer: Address,
        txn: XandTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        unimplemented!()
    }

    async fn get_transaction_details(
        &self,
        id: &TransactionId,
    ) -> Result<Transaction, XandApiClientError> {
        self.txn_details.call(id.clone())
    }

    async fn get_transaction_history(
        &self,
        paging: Option<Paging>,
        filter: &TransactionFilter,
    ) -> Result<Paginated<Vec<Transaction>>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_balance(&self, address: &str) -> Result<Option<u128>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_current_block(&self) -> Result<Blockstamp, XandApiClientError> {
        unimplemented!()
    }

    async fn get_total_issuance(&self) -> Result<u64, XandApiClientError> {
        unimplemented!()
    }

    async fn get_address_transactions(
        &self,
        address: &str,
        paging: Option<Paging>,
    ) -> Result<TransactionHistoryPage, XandApiClientError> {
        unimplemented!()
    }

    async fn get_pending_create_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingCreateRequest>>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_pending_redeem_requests(
        &self,
        paging: Option<Paging>,
    ) -> Result<Paginated<Vec<PendingRedeemRequest>>, XandApiClientError> {
        unimplemented!()
    }

    async fn propose_action(
        &self,
        issuer: Address,
        admin_txn: AdministrativeTransaction,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        unimplemented!()
    }

    async fn vote_on_proposal(
        &self,
        issuer: Address,
        voting_txn: VoteProposal,
    ) -> Result<TransactionStatusStream, XandApiClientError> {
        let reply = self.vote_on_proposal.call((issuer, voting_txn));
        if let Ok(reply) = reply {
            Ok(reply.into())
        } else {
            Err(reply.unwrap_err())
        }
    }

    async fn get_proposal(&self, id: u32) -> Result<Proposal, XandApiClientError> {
        unimplemented!()
    }

    async fn get_all_proposals(&self) -> Result<Vec<Proposal>, XandApiClientError> {
        Ok(self.get_all_proposals.call(()))
    }

    async fn get_members(&self) -> Result<Vec<Address>, XandApiClientError> {
        Ok(vec![
            MEMBER_0.clone(),
            MEMBER_1.clone(),
            MEMBER_2.clone(),
            MEMBER_3.clone(),
        ])
    }

    async fn get_authority_keys(&self) -> Result<Vec<Address>, XandApiClientError> {
        Ok(vec![VALIDATOR_0.clone(), VALIDATOR_1.clone()])
    }

    async fn get_trustee(&self) -> Result<Address, XandApiClientError> {
        unimplemented!()
    }

    async fn get_allowlist(&self) -> Result<Vec<(Address, CidrBlock)>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_limited_agent(&self) -> Result<Option<Address>, XandApiClientError> {
        unimplemented!()
    }

    async fn get_validator_emission_rate(
        &self,
    ) -> Result<xand_api_client::ValidatorEmissionRate, XandApiClientError> {
        unimplemented!()
    }

    async fn get_validator_emission_progress(
        &self,
        address: Address,
    ) -> Result<xand_api_client::ValidatorEmissionProgress, XandApiClientError> {
        unimplemented!()
    }

    async fn get_pending_create_request_expire_time(&self) -> Result<u32, XandApiClientError> {
        unimplemented!()
    }
}

#[tokio::test]
async fn all_proposals_can_be_requested() {
    let proposals = vec![gen_proposal()];
    let proposal_ids = proposals.iter().map(|p| p.id).collect::<Vec<_>>();
    let mock_xapi_client = MockXandApiClient::from_proposals(proposals);

    let adapter = XandApiClientAdapter {
        client: mock_xapi_client,
    };
    let res = adapter.get_all_proposals().await.unwrap();
    assert_eq!(res[0].id, proposal_ids[0]);
    assert!(adapter.client.get_all_proposals.called());
}

#[tokio::test]
async fn votes_get_sorted_by_participant_type() {
    let proposals = vec![gen_proposal_with_voters()];
    let mock_xapi_client = MockXandApiClient::from_proposals(proposals);
    let adapter = XandApiClientAdapter {
        client: mock_xapi_client,
    };
    let res = adapter.get_all_proposals().await.unwrap();

    assert_eq!(res[0].validator_votes.yea_voters.len(), 2);
    assert_eq!(res[0].validator_votes.nay_voters.len(), 0);

    assert_eq!(res[0].member_votes.yea_voters.len(), 3);
    assert_eq!(res[0].member_votes.nay_voters.len(), 1);
}

#[tokio::test]
async fn can_vote_on_proposal() {
    const PROPOSAL_ID: u32 = 1;
    let txn = XandTransaction::RegisterMember(RegisterAccountAsMember {
        address: MEMBER_0.clone(),
        encryption_key: [0u8; 32].into(),
    });
    let txndeets = Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        MEMBER_0.clone(),
        TransactionStatus::Finalized,
        DateTime::from(SystemTime::now()),
    );

    let mock_xapi_client = MockXandApiClient {
        propose_action: Mock::new(Ok(Default::default())),
        vote_on_proposal: Mock::new(Ok(vec![Ok(TransactionUpdate {
            status: TransactionStatus::Committed,
            id: Default::default(),
        })])),
        get_all_proposals: Mock::new(vec![]),
        txn_details: Mock::new(Ok(txndeets)),
    };
    let mut adapter = XandApiClientAdapter {
        client: mock_xapi_client,
    };
    adapter
        .submit_vote(MEMBER_0.to_string(), Vote::Accept { id: PROPOSAL_ID })
        .await
        .unwrap();
    assert!(adapter.client.vote_on_proposal.called_with((
        MEMBER_0.clone(),
        VoteProposal {
            id: PROPOSAL_ID,
            vote: true
        }
    )));
    assert!(adapter
        .client
        .txn_details
        .called_with(TransactionId::default()));
}
