# Xand Network Voting CLI Display Module

### Description

The Display module is meant to contain our logic for validating, formatting, and rendering tables with `prettytable` to show the results of some CLI action.

### Structure

`Models/ViewModel` -> A structure that contains data that can be used to render a table.  Relevant only for the contexts it is created in, and should be atomic.

`Tables/Component` -> A reusable renderable structure for use in constructing result tables.  There are two types of components - those that render to a String directly, and those that create a (preferably unformatted and with no title) `prettytable` Table.

`Tables/View` ->  A final table result to be rendered to the terminal to show the user all relevant information about their action.  This is the highest level display structure.


### Notes

Per https://github.com/phsym/prettytable-rs/issues/45 if you nest a table (like using a table component instead of a string component in a view), that table will lose text formatting.  Please keep this limitation in mind.
