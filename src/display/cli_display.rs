use prettytable::Table;

use crate::errors::Error;

pub struct CliDisplay {}

impl CliDisplay {
    /// Prints string to stderr
    pub fn print_error(msg: String) {
        eprintln!("{}", msg);
    }

    /// Prints debug information of an error to stderr
    pub fn print_cli_error(e: &Error) {
        let s = format!("{:#?}", e);
        CliDisplay::print_error(s);
    }

    /// Prints string to stdout
    pub fn print_line(msg: String) {
        println!("{}", msg);
    }

    /// Prints prettytable table to stdout
    pub fn print_table(printable_table: Box<dyn PrintableTable>) {
        let table = printable_table.get_pretty_table();
        table.printstd();
    }
}

/// Trait which any struct needs to implement to be displayed as a result
/// Used to create table layouts that can print from multiple data struct props
pub trait PrintableTable {
    fn get_pretty_table(&self) -> Table;
}
