use std::fmt::{Display, Formatter};

#[derive(Debug, Copy, Clone)]
pub enum Status {
    Open,
    Accepted,
    Rejected,
    Invalid,
}

impl Display for Status {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self)
    }
}
