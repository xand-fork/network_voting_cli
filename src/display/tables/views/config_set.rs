use prettytable::{format, Row, Table};

use crate::display::cli_display::PrintableTable;
use crate::display::models::config::CliConfigViewModel;
use crate::display::tables::components::config_summary::{
    ConfigSummaryComponent, ConfigSummaryComponentProps,
};

const NO_DEFAULT_CONFIG_MESSAGE: &str = "No default config was found.";
const TITLE: &str = "Network Voting CLI Configuration Update";

pub struct ConfigSetTableProps {
    pub old_config: Option<CliConfigViewModel>,
    pub new_config: CliConfigViewModel,
    pub truncate_addresses: bool,
}

pub struct ConfigSetTable {
    props: ConfigSetTableProps,
}

impl ConfigSetTable {
    pub fn new(props: ConfigSetTableProps) -> Self {
        ConfigSetTable { props }
    }

    fn create_container_table_with_header(&self) -> Table {
        let mut container = Table::new();
        container.set_format(*format::consts::FORMAT_DEFAULT);
        container.add_row(row![H2bc=>TITLE]);
        container.add_row(row![bFr->"Old Config", bFg->"New Config",]);
        container
    }

    fn create_old_config_inner_table(&self) -> Table {
        match self.props.old_config.clone() {
            Some(c) => ConfigSummaryComponent::new(ConfigSummaryComponentProps {
                config: c,
                table_format: None,
                truncate_addresses: self.props.truncate_addresses,
            })
            .get_pretty_table(),
            None => {
                let mut table = Table::new();
                table.add_row(row![NO_DEFAULT_CONFIG_MESSAGE]);
                table
            }
        }
    }

    fn create_new_config_inner_table(&self) -> Table {
        ConfigSummaryComponent::new(ConfigSummaryComponentProps {
            config: self.props.new_config.clone(),
            table_format: None,
            truncate_addresses: self.props.truncate_addresses,
        })
        .get_pretty_table()
    }

    fn create_configs_row(&self) -> Row {
        let mut config_row = Row::new(vec![]);
        let old_table = self.create_old_config_inner_table();
        config_row.add_cell(cell![old_table]);
        let new_table = self.create_new_config_inner_table();
        config_row.add_cell(cell![new_table]);
        config_row
    }
}

impl PrintableTable for ConfigSetTable {
    fn get_pretty_table(&self) -> Table {
        let mut table = self.create_container_table_with_header();
        let config_row = self.create_configs_row();
        table.add_row(config_row);
        table
    }
}
