use crate::cli::models::action::Action;
use crate::cli::models::status::Status;
use crate::cli::models::votes::VotesData;

#[derive(Clone, Debug)]
pub struct ProposalData {
    pub id: u32,
    pub action: Option<Action>,
    pub member_votes: VotesData,
    pub validator_votes: VotesData,
    pub status: Status,
    pub proposer: String,
    pub expires: u32,
}
