use prettytable::{format, Table};

use crate::display::cli_display::PrintableTable;
use crate::display::models::config::CliConfigViewModel;
use crate::display::tables::components::config_summary::{
    ConfigSummaryComponent, ConfigSummaryComponentProps,
};

pub const TITLE: &str = "Network Voting CLI Config";

pub struct ConfigTableProps {
    pub config: CliConfigViewModel,
    pub truncate_addresses: bool,
}

pub struct ConfigTable {
    props: ConfigTableProps,
}

impl ConfigTable {
    pub fn new(props: ConfigTableProps) -> Self {
        ConfigTable { props }
    }
    fn create_config_summary_component(&self) -> ConfigSummaryComponent {
        ConfigSummaryComponent::new(ConfigSummaryComponentProps {
            config: self.props.config.clone(),
            table_format: Some(*format::consts::FORMAT_DEFAULT),
            truncate_addresses: self.props.truncate_addresses,
        })
    }
}

impl PrintableTable for ConfigTable {
    fn get_pretty_table(&self) -> Table {
        let mut table = Table::new();
        table.set_format(*format::consts::FORMAT_DEFAULT);
        let config_summary_component = self.create_config_summary_component();
        table.add_row(row![bcH2->TITLE]);
        table.add_row(config_summary_component.create_url_row());
        table.add_row(config_summary_component.create_issuer_row());
        table.add_row(config_summary_component.create_jwt_row());
        table
    }
}
