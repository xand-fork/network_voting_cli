use crate::display::cli_display::PrintableTable;
use crate::display::models::address::{AddressViewModel, AddressesViewModel};
use crate::display::models::votes::VotesViewModel;
use crate::display::tables::components::voters_summary::{
    VotesSummaryComponent, VotesSummaryComponentProps, NO_NAY_VOTERS_MESSAGE, NO_YEA_VOTERS_MESSAGE,
};

const YEA_VOTERS_ROW: usize = 0;
const NAY_VOTERS_ROW: usize = 2;

fn create_fake_votes() -> VotesViewModel {
    VotesViewModel {
        yea_voters: AddressesViewModel {
            addresses: vec![
                AddressViewModel {
                    full_address: "Alexander Hamilton".to_string(),
                    truncated_address: "Alexander Hamilton".to_string(),
                },
                AddressViewModel {
                    full_address: "Marquis de Lafayette".to_string(),
                    truncated_address: "Marquis de Lafayette".to_string(),
                },
            ],
            count: 2,
        },
        nay_voters: AddressesViewModel {
            addresses: vec![AddressViewModel {
                full_address: "Aaron Burr, Sir".to_string(),
                truncated_address: "Aaron Burr".to_string(),
            }],
            count: 1,
        },
    }
}

fn build_table_and_get_vote_content(
    votes: VotesViewModel,
    truncate_addresses: bool,
    row: usize,
) -> String {
    let view = VotesSummaryComponent::new(VotesSummaryComponentProps {
        votes,
        truncate_addresses,
    });
    let constructed_table = view.get_pretty_table();

    constructed_table
        .get_row(row)
        .unwrap()
        .get_cell(1)
        .unwrap()
        .get_content()
}

#[test]
fn displays_no_yea_voters() {
    let yea_votes =
        build_table_and_get_vote_content(VotesViewModel::default(), false, YEA_VOTERS_ROW);

    assert_eq!(yea_votes, NO_YEA_VOTERS_MESSAGE);
}

#[test]
fn displays_no_nay_voters() {
    let nay_votes =
        build_table_and_get_vote_content(VotesViewModel::default(), false, NAY_VOTERS_ROW);

    assert_eq!(nay_votes, NO_NAY_VOTERS_MESSAGE);
}

#[test]
fn correctly_displays_yea_voters() {
    let yea_votes = build_table_and_get_vote_content(create_fake_votes(), false, YEA_VOTERS_ROW);

    assert_eq!(
        yea_votes,
        "Alexander Hamilton\nMarquis de Lafayette".to_string()
    );
}

#[test]
fn correctly_displays_nay_voters() {
    let nay_votes = build_table_and_get_vote_content(create_fake_votes(), false, NAY_VOTERS_ROW);

    assert_eq!(nay_votes, "Aaron Burr, Sir".to_string());
}

#[test]
fn correctly_displays_truncated_nay_voters() {
    let nay_votes = build_table_and_get_vote_content(create_fake_votes(), true, NAY_VOTERS_ROW);

    assert_eq!(nay_votes, "Aaron Burr".to_string());
}
